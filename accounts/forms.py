from django import forms
from django.contrib.auth import authenticate, get_user_model
User = get_user_model()


class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        if username and password:
            user = authenticate(username=username, password=password)

            if not user:
                raise forms.ValidationError("L'utilisateur n'existe pas")

            if not user.check_password(password):
                raise forms.ValidationError("Mot de passe incorrect")

            if not user.is_active:
                raise forms.ValidationError("Cette utilisateur n'est pas active")

        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegisterForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput, required=True)
    first_name = forms.CharField(label='Prenom')
    last_name = forms.CharField(label='Nom')
    email = forms.EmailField(label='Adresse E-Mail', required=True)
    email_conf = forms.EmailField(label='Confirmation E-Mail', required=True)

    class Meta():
        model = User
        fields = ['username', 'password', 'first_name', 'last_name', 'email', 'email_conf']

    def clean_email_conf(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        email_conf = self.cleaned_data.get('email_conf')

        if email != email_conf:
            raise forms.ValidationError('Re confirmer votre E-Mail')
        email_qs = User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError('Cette E-Mail existe deja')

        return email
