$(document).ready(function () {
    $('.parallax').parallax();
    $('.modal').modal();
    $('.carousel').carousel();
    $('.scrollspy').scrollSpy({
        scrollOffset: 75
    });
    $('.pushpin-demo-nav').pushpin({
        top: $('#presentation').offset().top
    });
    $('.slider').slider({
        indicators: false
    });
    $('.materialboxed').materialbox();
    var contact_message = JSON.parse(document.getElementById('contact_message').textContent);
    if (contact_message) {
        Materialize.toast(contact_message.html, 3000, contact_message.classes);
    }
});
