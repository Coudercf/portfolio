import os
import yaml
import datetime
from django.core.management.base import BaseCommand
from mon_site.models import (Activite, ElemPortfolio, Competence, TypeCompetence, Categorie,
                             OutilVeille, TypeElem, TypeVeille, ContenueHtml, ContenueType)

class Command(BaseCommand):
    help = 'Deploiement du Portfolio !!'

    def handle(self, *args, **options):
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
        setup_yaml_file = os.path.join(BASE_DIR, "static", "setup.yml")

        with open(setup_yaml_file, "r", encoding="utf8") as file:
            yml_data = yaml.load(file)

        for activite in yml_data["Activite"].values():
            categorie = Categorie.objects.filter(libelle=activite["categorie"])
            if categorie:
                activite["categorie"] = categorie.first()
                if activite["date_debut"]:
                    date_debut = activite["date_debut"].split("-")
                    activite["date_debut"] = datetime.date(year=int(date_debut[0]), month=int(date_debut[1]), day=int(date_debut[2]))
                if activite["date_fin"]:
                    date_fin = activite["date_fin"].split("-")
                    activite["date_fin"] = datetime.date(year=int(date_fin[0]), month=int(date_fin[1]), day=int(date_fin[2]))
                qs_activite = Activite.objects.filter(**activite)
                if not qs_activite:
                    new_activite = Activite.objects.create(**activite)
                    print(new_activite)
                else:
                    print("Acitivite %s existe deja" % activite["libelle"])
            else:
                print("Categorie %s n'existe pas, migration requise" % activite["categorie"])

        for competence in yml_data["Competence"].values():
            type_competence = TypeCompetence.objects.filter(libelle=competence["type_competence"])
            if type_competence:
                competence["type_competence"] = type_competence.first()
                qs_competence = Competence.objects.filter(**competence)
                if not qs_competence:
                    new_competence = Competence.objects.create(**competence)
                    print(new_competence)
                else:
                    print("Competence %s existe déjà" % competence["libelle"])
            else:
                print("Type Competence %s n'existe pas, migration requise" % competence["type_competence"])

        for experience in yml_data["Experience"].values():
            type_elem = TypeElem.objects.filter(libelle=experience["type_elem"])
            if type_elem:
                experience["type_elem"] = type_elem.first()

                if experience["date_debut"]:
                    date_debut = experience["date_debut"].split("-")
                    experience["date_debut"] = datetime.date(year=int(date_debut[0]), month=int(date_debut[1]), day=int(date_debut[2]))
                if experience["date_fin"]:
                    date_fin = experience["date_fin"].split("-")
                    experience["date_fin"] = datetime.date(year=int(date_fin[0]), month=int(date_fin[1]), day=int(date_fin[2]))
                qs_experience = ElemPortfolio.objects.filter(**experience)
                if not qs_experience:
                    new_experience = ElemPortfolio.objects.create(**experience)
                    print(new_experience)
                else:
                    print("Experience %s existe déjà" % experience["titre"])
            else:
                print("Type Elem %s n'existe pas, migration requise" % experience["type_elem"])

        for formation in yml_data["Formation"].values():
            type_elem = TypeElem.objects.filter(libelle=formation["type_elem"])
            if type_elem:
                formation["type_elem"] = type_elem.first()

                if formation["date_debut"]:
                    date_debut = formation["date_debut"].split("-")
                    formation["date_debut"] = datetime.date(year=int(date_debut[0]), month=int(date_debut[1]), day=int(date_debut[2]))
                if formation["date_fin"]:
                    date_fin = formation["date_fin"].split("-")
                    formation["date_fin"] = datetime.date(year=int(date_fin[0]), month=int(date_fin[1]), day=int(date_fin[2]))
                qs_formation = ElemPortfolio.objects.filter(**formation)
                if not qs_formation:
                    new_formation = ElemPortfolio.objects.create(**formation)
                    print(new_formation)
                else:
                    print("formation %s existe déjà" % formation["titre"])
            else:
                print("Type Elem %s n'existe pas, migration requise" % formation["type_elem"])

        for type_veille, info in yml_data["OutilVeille"].items():
            type_veille = TypeVeille.objects.filter(libelle=type_veille)
            if type_veille:
                for outil in info.values():
                    outil["type_outil"] = type_veille.first()
                    qs_outil = OutilVeille.objects.filter(**outil)
                    if not qs_outil:
                        new_outil = OutilVeille.objects.create(**outil)
                        print(new_outil)
                    else:
                        print("Outil %s existe déjà" % outil["libelle"])
            else:
                print("Type Veille %s n'existe pas, migration requise" % type_veille)

        for contenue in yml_data["Contenue"].values():
            activite = Activite.objects.filter(libelle=contenue["activite"])
            if activite:
                contenue["activite"] = activite.first()
                type_contenue = ContenueType.objects.filter(libelle=contenue["c_type"])
                if type_contenue:
                    contenue["c_type"] = type_contenue.first()
                    if contenue["c_type"].libelle == "Html":
                        qs_contenue = ContenueHtml.objects.filter(**contenue)
                        if not qs_contenue:
                            new_contenue = ContenueHtml.objects.create(**contenue)
                            print(new_contenue)
                        else:
                            print("Contenue %s existe déjà" % contenue)
                else:
                    print("Type Contenue %s n'existe pas, migration requise" % contenue["c_type"])
            else:
                print("Activite %s n'existe pas" % contenue["activite"])
