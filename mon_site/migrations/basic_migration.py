from __future__ import unicode_literals
from django.db import migrations


def forwards_func(apps, schema_editor):
    Categorie = apps.get_model("mon_site", "Categorie")
    TypeCompetence = apps.get_model("mon_site", "TypeCompetence")
    TypeElem = apps.get_model("mon_site", "TypeElem")
    TypeVeille = apps.get_model("mon_site", "TypeVeille")
    ContenueType = apps.get_model("mon_site", "ContenueType")
    db_alias = schema_editor.connection.alias

    Categorie.objects.using(db_alias).bulk_create([
        Categorie(libelle="STMicroelectronics", classement=1),
        Categorie(libelle="Bellecour-Ecole", classement=2),
        Categorie(libelle="Sciences-U", classement=3),
        Categorie(libelle="Personnelle", classement=4)
    ])

    TypeCompetence.objects.using(db_alias).bulk_create([
        TypeCompetence(libelle="Langages", classement=1),
        TypeCompetence(libelle="Frameworks", classement=2),
        TypeCompetence(libelle="Moteur de Jeu", classement=3),
        TypeCompetence(libelle="Gestion Projet", classement=4),
        TypeCompetence(libelle="Multimédia", classement=5),
        TypeCompetence(libelle="Admin Système", classement=6)
    ])

    TypeElem.objects.using(db_alias).bulk_create([
        TypeElem(libelle="formation"),
        TypeElem(libelle="experience"),
    ])

    TypeVeille.objects.using(db_alias).bulk_create([
        TypeVeille(libelle="Technologique"),
        TypeVeille(libelle="Juridique"),
    ])

    ContenueType.objects.using(db_alias).bulk_create([
        ContenueType(libelle="Image"),
        ContenueType(libelle="Text"),
        ContenueType(libelle="Titre"),
        ContenueType(libelle="PDF"),
        ContenueType(libelle="Fichier"),
        ContenueType(libelle="Html")
    ])


def reverse_func(apps, schema_editor):
    Categorie = apps.get_model("mon_site", "Categorie")
    TypeCompetence = apps.get_model("mon_site", "TypeCompetence")
    TypeElem = apps.get_model("mon_site", "TypeElem")
    TypeVeille = apps.get_model("mon_site", "TypeVeille")
    ContenueType = apps.get_model("mon_site", "ContenueType")
    db_alias = schema_editor.connection.alias
    Categorie.objects.using(db_alias).filter(libelle="PPE").delete()
    Categorie.objects.using(db_alias).filter(libelle="Entreprise").delete()
    Categorie.objects.using(db_alias).filter(libelle="Jeux").delete()
    Categorie.objects.using(db_alias).filter(libelle="Personnelle").delete()
    TypeCompetence.objects.using(db_alias).filter(libelle="Gestion Projet").delete()
    TypeCompetence.objects.using(db_alias).filter(libelle="Moteur de Jeu").delete()
    TypeCompetence.objects.using(db_alias).filter(libelle="Développement d'Application").delete()
    TypeCompetence.objects.using(db_alias).filter(libelle="Multimédia").delete()
    TypeElem.objects.using(db_alias).filter(libelle="formation").delete()
    TypeElem.objects.using(db_alias).filter(libelle="experience").delete()
    TypeVeille.objects.using(db_alias).filter(libelle="Technologique").delete()
    TypeVeille.objects.using(db_alias).filter(libelle="Juridique").delete()
    ContenueType.objects.using(db_alias).filter(libelle="Image").delete()
    ContenueType.objects.using(db_alias).filter(libelle="Text").delete()
    ContenueType.objects.using(db_alias).filter(libelle="Titre").delete()
    ContenueType.objects.using(db_alias).filter(libelle="PDF").delete()
    ContenueType.objects.using(db_alias).filter(libelle="Fichier").delete()
    ContenueType.objects.using(db_alias).filter(libelle="Html").delete()


class Migration(migrations.Migration):
    dependencies = [('mon_site', '0001_initial')]
    operations = [migrations.RunPython(forwards_func, reverse_func)]
