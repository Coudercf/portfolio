from django import forms


class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True, label="Nom")
    contact_email = forms.EmailField(required=True, label="Email")
    content = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={"style":"height: 150px"}),
        label="Message"
    )
