from django.db import models


class TypeCompetence(models.Model):
    classement = models.IntegerField(default=0)
    libelle = models.CharField(max_length=50)
    percent = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.libelle


class Competence(models.Model):
    type_competence = models.ForeignKey(TypeCompetence, models.CASCADE, null=True, blank=True)
    image = models.CharField(max_length=200, null=True, blank=True)
    libelle = models.CharField(max_length=50, null=True, blank=True)
    projet = models.CharField(max_length=200, null=True, blank=True)
    star = models.CharField(max_length=5, null=True, blank=True)

    def __str__(self):
        return self.libelle
