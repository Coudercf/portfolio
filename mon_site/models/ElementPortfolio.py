from django.db import models


class TypeElem(models.Model):
    libelle = models.CharField(max_length=50)

    def __str__(self):
        return self.libelle


class ElemPortfolio(models.Model):
    type_elem = models.ForeignKey(TypeElem, models.CASCADE)
    icon = models.CharField(max_length=200, null=True, blank=True)
    titre = models.CharField(max_length=100, null=True, blank=True)
    sous_titre = models.CharField(max_length=100, null=True, blank=True)
    date_debut = models.DateField(null=True, blank=True)
    date_fin = models.DateField(null=True, blank=True)
    lieux = models.CharField(max_length=300, null=True, blank=True)
    content = models.TextField(null=True, blank=True)

    def __str__(self):
        return "%s - %s" % (self.type_elem.libelle.capitalize(), self.titre)
