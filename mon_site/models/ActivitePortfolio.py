import os
from django.db import models


class Categorie(models.Model):
    classement = models.IntegerField(default=True)
    libelle = models.CharField(max_length=100)

    def __str__(self):
        return self.libelle


class Activite(models.Model):
    libelle = models.CharField(max_length=300, blank=True)
    libelle_color = models.CharField(max_length=20, default="black", blank=True)
    categorie = models.ForeignKey(Categorie, models.CASCADE)
    resume = models.TextField(null=True, blank=True)
    miniature = models.CharField(max_length=200, null=True, blank=True)
    date_debut = models.DateField(null=True, blank=True)
    date_fin = models.DateField(null=True, blank=True)

    def __str__(self):
        if self.libelle:
            return self.libelle
        else:
            return self.resume[:10]


class ContenueType(models.Model):
    libelle = models.CharField(max_length=300)

    def __str__(self):
        return self.libelle


class Contenue(models.Model):
    activite = models.ForeignKey(Activite, models.CASCADE)
    c_type = models.ForeignKey(ContenueType, models.CASCADE)
    rang = models.IntegerField(null=False)


class ContenueImage(Contenue):
    content = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.content.split(os.sep)[-1]


class ContenueText(Contenue):
    content = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.content[:20]


class ContenueFichier(Contenue):
    libelle = models.CharField(max_length=200, null=True, blank=True)
    link = models.CharField(max_length=200, null=True, blank=True)


class ContenueHtml(Contenue):
    content = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.content
