from django.db import models


class TypeVeille(models.Model):
    libelle = models.CharField(max_length=50)

    def __str__(self):
        return self.libelle


class OutilVeille(models.Model):
    type_outil = models.ForeignKey(TypeVeille, models.CASCADE, null=True, blank=True)
    libelle = models.CharField(max_length=100, null=True, blank=True)
    image = models.CharField(max_length=200, null=True, blank=True)
    link = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.libelle
