from .views import projet_personnel_encadre
from django.conf.urls import url

urlpatterns = [
    url(r'^$', projet_personnel_encadre, name="portfolio")
]
