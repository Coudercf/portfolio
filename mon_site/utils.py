import json
import urllib.parse
import urllib.request
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import get_template
from mon_site.forms import ContactForm


def contact_request(request):
    form_message = None
    if request.method == 'POST':
        form_contact = ContactForm(request.POST)

        if form_contact.is_valid():
            recaptcha_response = request.POST.get('g-recaptcha-response')
            url = 'https://www.google.com/recaptcha/api/siteverify'
            values = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY_SERVICE,
                'response': recaptcha_response
            }
            data = urllib.parse.urlencode(values).encode()
            req = urllib.request.Request(url, data=data)
            response = urllib.request.urlopen(req)
            result = json.loads(response.read().decode())
            if result["success"]:
                contact_name = request.POST.get('contact_name', '')
                contact_email = request.POST.get('contact_email', '')
                form_content = request.POST.get('content', '')
                template = get_template('mon_site/contact_template.txt')
                context = {
                    'contact_name': contact_name,
                    'contact_email': contact_email,
                    'form_content': form_content,
                }
                content = template.render(context)

                send_mail(
                    "Portfolio contact !",
                    content,
                    contact_email,
                    ['florian.couderc.pro@gmail.com'],
                )
                form_message = {
                    "html": "Message Envoyé !",
                    "classes": "green"
                }
            else:
                form_message = {
                    "html": "Captcha Invalid",
                    "classes": "red"
                }
    form_contact = ContactForm()
    return form_contact, form_message
