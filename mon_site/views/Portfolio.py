from django.shortcuts import render
from mon_site.utils import contact_request
from operator import attrgetter
from itertools import chain
from mon_site.models import (Activite, ContenueImage, ContenueText, ElemPortfolio,
                             Competence, TypeCompetence, Categorie, OutilVeille, Contenue, ContenueHtml)


def projet_personnel_encadre(request):

    contact, contact_message = contact_request(request)

    qs_activite = Activite.objects.all()
    qs_contenue = Contenue.objects.all()
    qs_formation = ElemPortfolio.objects.filter(type_elem__libelle="formation").order_by("-date_debut")
    qs_experience = ElemPortfolio.objects.filter(type_elem__libelle="experience").order_by("-date_debut")
    qs_competence = Competence.objects.all().order_by("-star")
    qs_categories = Categorie.objects.all().order_by("classement")
    qs_type_competence = TypeCompetence.objects.all().order_by("classement")
    qs_outil_veille = OutilVeille.objects.all()

    activite = {}
    for act in qs_activite:
        qs_image = ContenueImage.objects.filter(activite=act)
        qs_text = ContenueText.objects.filter(activite=act)
        qs_html = ContenueHtml.objects.filter(activite=act)
        activite[act] = sorted(chain(qs_image, qs_text, qs_html), key=attrgetter("rang"))

    context = {
        "activite": activite,
        "categorie": qs_categories,
        "formation_elem": qs_formation,
        "experience_elem": qs_experience,
        "list_competence": qs_competence,
        "type_competence": qs_type_competence,
        "contact_form": contact,
        "contact_message": contact_message,
        "outil_veille": qs_outil_veille,
    }

    return render(request, "mon_site/Portfolio.html", context)
