from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Categorie)
admin.site.register(Activite)
admin.site.register(Contenue)
admin.site.register(ContenueType)
admin.site.register(ContenueImage)
admin.site.register(ContenueText)
admin.site.register(ContenueFichier)
admin.site.register(ContenueHtml)

admin.site.register(TypeElem)
admin.site.register(ElemPortfolio)
admin.site.register(Competence)
admin.site.register(TypeCompetence)
admin.site.register(TypeVeille)
admin.site.register(OutilVeille)
